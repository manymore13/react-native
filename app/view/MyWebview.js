import React, { Component } from 'react';
import {
    StyleSheet,
    WebView

} from 'react-native';


export default class MyWebView extends Component {
	constructor(props){
        super(props);
    }
  render() {
    return (
      <View style={{flex:1}}>
        <WebView style={styles.webview_style} 
          url={this.props.url}
          startInLoadingState={true}
          domStorageEnabled={true}
          javaScriptEnabled={true}
          >
        </WebView>
      </View>
    );
  }
}

var styles = StyleSheet.create({
    webview_style:{  
       backgroundColor:'#00ff00',   
    }
});