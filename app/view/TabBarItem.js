import React, { Component } from 'react';

import {
	AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
} from 'react-native';

export default class TabBarItem extends Component{
	constructor(props){
        super(props);
    }
    render(){
    	var title = this.props.title;
    	var image = this.props.image;

    	return(
    		<TouchableHighlight style={styles.tabBarItemContain}
    			underlayColor="#b5b5b5" 
    			onPress={this.props.onPress}>
    			<View style={styles.tabBarItem}>
    				<Text style={styles.title}>{title}</Text>
    				<Image style={styles.image} source={image}/>
    			</View>
    		</TouchableHighlight>
    	);
    }

}

 var styles = StyleSheet.create(
	{

        tabBarItemContain:{
            flex:2,
            backgroundColor:"#fff",
            justifyContent:'center',
            alignContent:'center',
            alignItems:'center'
        },
		tabBarItem:{
			flexDirection: 'column',
        	flexWrap: 'wrap',
        	flex: 1,
            justifyContent:'space-around'
		},

		image: {
	        width: 45,
	        height: 45,
	        marginTop: 5,
	        resizeMode: Image.resizeMode.stretch,
    	},
	    title: {
            textAlign:'center',
            fontWeight:'bold',
	        fontSize: 15,
            justifyContent:'center',
            alignContent:'center',

	    }	

	}

);