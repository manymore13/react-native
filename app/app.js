
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Navigator,
    ToastAndroid,
    TouchableHighlight,

} from 'react-native';

import TabBarItem from './view/TabBarItem';
import MyWebView  from './view/MyWebview';

class NavButton extends React.Component {
  render() {
    return (
      <TouchableHighlight
        style={styles.button}
        underlayColor="#B5B5B5"
        onPress={this.props.onPress}>
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableHighlight>
    );
  }
}

export default class App extends Component{
	constructor(props){
        super(props);
    }

    render() {
        return (
          <Navigator
            style={styles.container}
            initialRoute={{ message: '初始页面', index:0,url:'www.baidu.com'}}
            renderScene={ (route, navigator) => {this.renderContent(route.message,route.url)}}
            configureScene={(route) => {
              if (route.sceneConfig) {
                return route.sceneConfig;
              }
              return Navigator.SceneConfigs.FloatFromBottom;
            }}
            navigationBar = {this._navigationBar(navigator)}
          />
        );
    }

    renderContent(message,url)
    {
       // ToastAndroid.show(message + url,ToastAndroid.SHORT);

        return (
                                <NavButton text = {message}/>

            
                );
    }


    _navigationBar(navigator)
    {
        return (
            <TabBar  navigator = {navigator}/>           
        );    
    }
}


function _doPress(index)
{
    ToastAndroid.show('收到点击事件'+index,ToastAndroid.SHORT);
}
//导航栏
class TabBar extends Component{
    constructor(props){
        super(props);
    }
   
    render(){

        
        return(
            
            <View style={styles.tabs }>
                <TabBarItem 
                    title = {'百度'}
                    image =  {require('./film.png')}
                    onPress = {()=> {this.props.navigator.replace({
                        message:'百度',
                        url : 'www.baidu.com',
                        sceneConfig: Navigator.SceneConfigs.FadeAndroid,

                    })}

                    }
                />
                <TabBarItem 
                    title = {'影院'}
                    image =  {require('./cinema.png')}
                   
                    onPress = {()=> {this.props.navigator.replace({
                        message:'影院',
                        sceneConfig: Navigator.SceneConfigs.FadeAndroid,
                    })}
                    }
                />
                <TabBarItem 
                    title = {'我'}
                    image =  {require('./me.png')}
                   
                    onPress = {()=> {this.props.navigator.replace({
                        message:'我',
                        sceneConfig: Navigator.SceneConfigs.FadeAndroid,

                    })}
                    }
                />
                
            </View>
           /* <NavButton 
                text = {this.props.text}
            />*/
        )
    }
}

const styles = StyleSheet.create({
    tabs:{
        flexDirection:"row",
        justityContent:'center',
        alignContent:'center'
   },
   container: {
    flex: 1,
   },
   messageText: {
    fontSize: 17,
    fontWeight: '500',
    padding: 15,
    marginTop: 50,
    marginLeft: 15,
  },
  buttonText:{
    fontSize:22,
    fontWeight:'bold',
    textAlign:'center',
  },
  button: {
    justityContent:'center',
    alignItem:'center',
    backgroundColor: 'white',
    padding: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#CDCDCD',
  },
});